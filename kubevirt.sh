# KubeVirt

oc adm policy add-scc-to-user privileged -n kube-system -z kubevirt-privileged
oc adm policy add-scc-to-user privileged -n kube-system -z kubevirt-controller
oc adm policy add-scc-to-user privileged -n kube-system -z kubevirt-apiserver

kubectl create configmap -n kube-system kubevirt-config     --from-literal debug.useEmulation=true

# apply the KubeVirt configuration, adjust RELEASE for the current version
RELEASE=v0.11.0
kubectl apply -f https://github.com/kubevirt/kubevirt/releases/download/${RELEASE}/kubevirt.yaml
curl -L -o virtctl https://github.com/kubevirt/kubevirt/releases/download/$RELEASE/virtctl-$RELEASE-linux-amd64
chmod +x virtctl

# Test VM

kubectl create -f https://raw.githubusercontent.com/kubevirt/demo/master/manifests/vm.yaml
./virtctl start testvm    
